function create_gram7(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=7
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-7.txt'];
control = [temp '/control-7.txt'];
targetQ7 = textread(target, '%s ');
controlQ7 = textread(control, '%s ');

Gram_7=setdiff(union(targetQ7,controlQ7),entries);
Gram_7_vector=zeros(length(entries),length(Gram_7));
[WC,WIA,WIB]=intersect(entries,controlQ7);
for i=1:length(WIB)-1
   [uniquecontrolQ7,WIA7,WIB7]=unique(controlQ7(WIB(i)+1:WIB(i+1)-1));
   if(length(WIB7)==0)
      Gram_7_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ7)
         Freq1(k)=length(find(WIB7==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ7,Gram_7);
      Gram_7_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ7,WIA7,WIB7]=unique(controlQ7(WIB(end)+1:end));
if(length(WIB7)==0)
   Gram_7_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ7)
      Freq1(k)=length(find(WIB7==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ7,Gram_7);
   Gram_7_vector(WIA(end),IB)=Freq1(IA);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ7);
for i=1:length(FIB)-1
   [FC7,FIA7,FIB7]=unique(targetQ7(FIB(i)+1:FIB(i+1)-1));
   if(length(FIB7)==0)
      Gram_7_vector(FIA(i),:)=0;
   else
      for k=1:length(FC7)
         Freq1(k)=length(find(FIB7==k));
      end
      [C,IA,IB]=intersect(FC7,Gram_7);
      Gram_7_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC7,FIA7,FIB7]=unique(targetQ7(FIB(end)+1:end));
if(length(FIB7)==0)
   Gram_7_vector(FIA(i),:)=0;
else
   for k=1:length(FC7)
      Freq1(k)=length(find(FIB7==k));
   end
   [C,IA,IB]=intersect(FC7,Gram_7);
   Gram_7_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_7);
Gram_7_weights = repmat(weights,1,rows);
Gram_7_vector = Gram_7_vector.*Gram_7_weights;

savename = [temp '/Gram_7'];
save(savename,'Gram_7','Gram_7_vector');



