function main_classfication_featureselection(alpha, mNumber, temp, minq, maxq, ScIn, maxl)

% final_alpha = 0.5;
simi_of_alpha(alpha, temp, minq, maxq, ScIn, maxl);

if mNumber == 1
    for i = minq:maxq
        Creat_Kernel('LK', num2str(i), temp);
        NAUC2=SVM_single_Kernel('LK', num2str(i), temp);
        auc2(i)=mean(NAUC2);
        featureselect2010('LK', num2str(i), temp);
    end
    featurelist(temp, 'LK', minq, maxq);

elseif mNumber == 2
    for i = minq:maxq
        Creat_Kernel('LKR', num2str(i), temp);
        NAUC1=SVM_single_Kernel('LKR', num2str(i), temp);
        auc1(i)=mean(NAUC1);
        featureselect2010('LKR', num2str(i), temp);
    end
    featurelist(temp, 'LKR', minq, maxq);

else
    for i = minq:maxq
        Creat_Kernel('LK', num2str(i), temp);
        Creat_Kernel('LKR', num2str(i), temp);
         
        NAUC1=SVM_single_Kernel('LK', num2str(i), temp);
        auc1(i)=mean(NAUC1);
        
        NAUC2=SVM_single_Kernel('LKR', num2str(i), temp);
        auc2(i)=mean(NAUC2);
        
        featureselect2010('LK', num2str(i), temp);
        featureselect2010('LKR', num2str(i), temp);
    end
    featurelist(temp, 'LK', minq, maxq);
    featurelist(temp, 'LKR', minq, maxq);
end
