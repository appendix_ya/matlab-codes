function generate_partlinkage8(temp)

%% the similarity of linkages from 8-gram.
load([temp '/Gram_8.mat']);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  remove the "";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gram8 = Gram_8;
for j = 1: length(gram8)
   gram8{j} = gram8{j}(2:end-1);
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% neglect the first redundant chemical bond;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:length(gram8)
   aa = findstr(']',gram8{k});
   bb = findstr('+',gram8{k});
   layer8(k) = str2num(gram8{k}(1));
   if length(aa)==8
      gram8{k} =gram8{k}(aa(1)+1:end);
   else
      gram8{k} = gram8{k}(bb(1)+1:end);     
   end
   gram8_layer{k} = [num2str(layer8(k)) '-' gram8{k} ];
end
 
layer8 = layer8';
gram8_layer  = gram8_layer';

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% unify the structure of 8-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(gram8)
   aa = findstr('<',gram8{i});
   bb = findstr('>',gram8{i});
   for j=0:100
      if length(aa)==j&&length(bb)==j
         Gram_shape(i)=j;
      end
   end
end

for i = 1:length(gram8)
   gram8{i}=strrep(gram8{i},'<','');
   gram8{i}=strrep(gram8{i},'>','');
   aa = findstr('[',gram8{i});
   bb = findstr(']',gram8{i});
   Gram_G1{i} = gram8{i}(1:aa(1)-1);
   Gram_Ed1{i} = gram8{i}(aa(1)+1:bb(1)-1);
   Gram_G2{i} = gram8{i}(bb(1)+1:aa(2)-1);
   Gram_Ed2{i} = gram8{i}(aa(2)+1:bb(2)-1);
   Gram_G3{i} = gram8{i}(bb(2)+1:aa(3)-1);
   Gram_Ed3{i} = gram8{i}(aa(3)+1:bb(3)-1);
   Gram_G4{i} = gram8{i}(bb(3)+1:aa(4)-1);
   Gram_Ed4{i} = gram8{i}(aa(4)+1:bb(4)-1);
   Gram_G5{i} = gram8{i}(bb(4)+1:aa(5)-1);
   Gram_Ed5{i} = gram8{i}(aa(5)+1:bb(5)-1);
   Gram_G6{i} = gram8{i}(bb(5)+1:aa(6)-1);
   Gram_Ed6{i} = gram8{i}(aa(6)+1:bb(6)-1);
   Gram_G7{i} = gram8{i}(bb(6)+1:aa(7)-1);
   Gram_Ed7{i} = gram8{i}(aa(7)+1:bb(7)-1);
   Gram_G8{i} = gram8{i}(bb(7)+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_Ed3 = Gram_Ed3';
Gram_G4 = Gram_G4';
Gram_Ed4 = Gram_Ed4';
Gram_G5 = Gram_G5';
Gram_Ed5 = Gram_Ed5';
Gram_G6 = Gram_G6';
Gram_Ed6 = Gram_Ed6';
Gram_G7 = Gram_G7';  
Gram_Ed7 = Gram_Ed7';
Gram_G8 = Gram_G8';  
Gram_shape = Gram_shape';

[unique_gram8_layer,IA] = unique(gram8_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGramlayer_Ed3 = Gram_Ed3(IA);
UGramlayer_G4 = Gram_G4(IA);
UGramlayer_Ed4 = Gram_Ed4(IA);
UGramlayer_G5 = Gram_G5(IA);
UGramlayer_Ed5 = Gram_Ed5(IA);
UGramlayer_G6 = Gram_G6(IA);
UGramlayer_Ed6 = Gram_Ed6(IA);
UGramlayer_G7 = Gram_G7(IA);
UGramlayer_Ed7 = Gram_Ed7(IA);
UGramlayer_G8 = Gram_G8(IA);
UGram_layer = layer8(IA);
UGram_shape = Gram_shape(IA);


UGGL = union(UGramlayer_G8,union(UGramlayer_G7,union(UGramlayer_G6,union(UGramlayer_G5,union(union(UGramlayer_G3,UGramlayer_G4),union(UGramlayer_G1,UGramlayer_G2))))));
UEEdL = union(UGramlayer_Ed7,union(UGramlayer_Ed6,union(UGramlayer_Ed5,union(union(UGramlayer_Ed1,UGramlayer_Ed2),union(UGramlayer_Ed3,UGramlayer_Ed4)))));

savename = [temp '/partgram8_linkage_layer'];
save(savename,'gram8_layer','layer8','unique_gram8_layer','Gram_G1','Gram_Ed1','Gram_G2', ...
   'Gram_Ed2','Gram_G3','Gram_Ed3','Gram_G4','Gram_Ed4','Gram_G5','Gram_Ed5','Gram_G6', ...
   'Gram_Ed6','Gram_G7','Gram_Ed7','Gram_G8','UGram_layer','UGramlayer_G1', ...
   'UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2','UGramlayer_G3','UGramlayer_Ed3', ...
   'UGramlayer_G4','UGramlayer_Ed4','UGramlayer_G5','UGramlayer_Ed5','UGramlayer_G6', ...
   'UGramlayer_Ed6','UGramlayer_G7','UGramlayer_Ed7','UGramlayer_G8','UGram_shape','UGGL','UEEdL');


