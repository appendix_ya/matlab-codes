function remove_gram5(temp)
%clear the redundant qgrams in the list
load([temp '/Gram_5.mat']);

for i = 1: length(Gram_5)
   Gram_5{i} = Gram_5{i}(2:end-1);
end
for j = 5
   gram = Gram_5;
   for k = 1:length(gram)
      aa = findstr(']',gram{k});
      bb = findstr('+',gram{k});
      layer = gram{k}(1);
      if length(aa)==j
         gram{k} =gram{k}(aa(1)+1:end);
      else
         gram{k} = gram{k}(bb(1)+1:end);     
      end
      gram_layer{j}{k} = [layer '-' gram{k} ];
   end
 
   Upart_qgram{j} = unique(gram_layer{j});
   Upart_VV=zeros(size(Gram_5_vector,1),length(Upart_qgram{j}));
   for i = 1: length(Upart_qgram{j})
      IN =  find(strcmp(Upart_qgram{j}{i},gram_layer{j}));
      Upart_VV(:,i)=sum(Gram_5_vector(:,IN),2);
   end
end


saveG = [temp '/gram_layer'];
save(saveG,'gram_layer');

saveU = [temp '/Upart_gram5_vector'];
save(saveU,'Upart_qgram','Upart_VV','Gram_5');

