function main(tmp_in, f)

ex2 = cellfun(@ex_func,tmp_in,'UniformOutput',0);
size_ex2 = cellfun(@length,ex2,'UniformOutput',0);
str_length = max(max(cell2mat(size_ex2)));
ex3 = cellfun(@(x) ex_func2(x,str_length),ex2,'uniformoutput',0)

ex4 = cell2mat(ex3);
fid = fopen(f,'a+');
for i = 1:size(ex4,1)
   fprintf(fid,'%s\n',ex4(i,:));
end
fclose(fid);
