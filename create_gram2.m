function create_gram2(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-2.txt'];
control = [temp '/control-2.txt'];
targetQ2 = textread(target, '%s ');
controlQ2 = textread(control, '%s ');

Gram_2=setdiff(union(targetQ2,controlQ2),entries);
Gram_2_vector=zeros(length(entries),length(Gram_2));
[WC,WIA,WIB]=intersect(entries,controlQ2);
for i=1:length(WIB)-1
    [uniquecontrolQ2,WIA2,WIB2]=unique(controlQ2(WIB(i)+1:WIB(i+1)-1));
    for k=1:length(uniquecontrolQ2)
          Freq1(k)=length(find(WIB2==k));
    end
    [C,IA,IB]=intersect(uniquecontrolQ2,Gram_2);
    Gram_2_vector(WIA(i),IB)=Freq1(IA);
end
[uniquecontrolQ2,WIA2,WIB2]=unique(controlQ2(WIB(end)+1:end));
for k=1:length(uniquecontrolQ2)
   Freq1(k)=length(find(WIB2==k));
end
[C,IA,IB]=intersect(uniquecontrolQ2,Gram_2);
Gram_2_vector(WIA(end),IB)=Freq1(IA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ2);
for i=1:length(FIB)-1
   [FC2,FIA2,FIB2]=unique(targetQ2(FIB(i)+1:FIB(i+1)-1));
   for k=1:length(FC2)
      Freq1(k)=length(find(FIB2==k));
   end
   [C,IA,IB]=intersect(FC2,Gram_2);
   Gram_2_vector(FIA(i),IB)=Freq1(IA);
end
[FC2,FIA2,FIB2]=unique(targetQ2(FIB(end)+1:end));
for k=1:length(FC2)
   Freq1(k)=length(find(FIB2==k));
end
[C,IA,IB]=intersect(FC2,Gram_2);
Gram_2_vector(FIA(end),IB)=Freq1(IA);
[rows,cols] = size(Gram_2);
Gram_2_weights = repmat(weights,1,rows);
Gram_2_vector = Gram_2_vector.*Gram_2_weights;

savename = [temp '/Gram_2'];
save(savename,'Gram_2','Gram_2_vector');



