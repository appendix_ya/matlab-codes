function generate_partlinkage6(temp)

%% the similarity of linkages from 6-gram.
load([temp '/Gram_6.mat']);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  remove the "";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gram6 = Gram_6;
   for j = 1: length(gram6)
      gram6{j} = gram6{j}(2:end-1);
   end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% neglect the first redundant chemical bond;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:length(gram6)
   aa = findstr(']',gram6{k});
   bb = findstr('+',gram6{k});
   layer6(k) = str2num(gram6{k}(1));
   if length(aa)==6
      gram6{k} =gram6{k}(aa(1)+1:end);
   else
      gram6{k} = gram6{k}(bb(1)+1:end);     
   end
   gram6_layer{k} = [num2str(layer6(k)) '-' gram6{k} ];
end
 
layer6 = layer6';
gram6_layer  = gram6_layer';

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% unify the structure of 6-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(gram6)
   aa = findstr('<',gram6{i});
   bb = findstr('>',gram6{i});
   for j=0:100
      if length(aa)==j&&length(bb)==j
         Gram_shape(i)=j;
      end
   end
end


for i = 1:length(gram6)
   gram6{i}=strrep(gram6{i},'<','');
   gram6{i}=strrep(gram6{i},'>','');
   aa = findstr('[',gram6{i});
   bb = findstr(']',gram6{i});
   Gram_G1{i} = gram6{i}(1:aa(1)-1);
   Gram_Ed1{i} = gram6{i}(aa(1)+1:bb(1)-1);
   Gram_G2{i} = gram6{i}(bb(1)+1:aa(2)-1);
   Gram_Ed2{i} = gram6{i}(aa(2)+1:bb(2)-1);
   Gram_G3{i} = gram6{i}(bb(2)+1:aa(3)-1);
   Gram_Ed3{i} = gram6{i}(aa(3)+1:bb(3)-1);
   Gram_G4{i} = gram6{i}(bb(3)+1:aa(4)-1);
   Gram_Ed4{i} = gram6{i}(aa(4)+1:bb(4)-1);
   Gram_G5{i} = gram6{i}(bb(4)+1:aa(5)-1);
   Gram_Ed5{i} = gram6{i}(aa(5)+1:bb(5)-1);
   Gram_G6{i} = gram6{i}(bb(5)+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_Ed3 = Gram_Ed3';
Gram_G4 = Gram_G4';
Gram_Ed4 = Gram_Ed4';
Gram_G5 = Gram_G5';
Gram_Ed5 = Gram_Ed5';
Gram_G6 = Gram_G6';
Gram_shape = Gram_shape';
  
  
[unique_gram6_layer,IA] = unique(gram6_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGramlayer_Ed3 = Gram_Ed3(IA);
UGramlayer_G4 = Gram_G4(IA);
UGramlayer_Ed4 = Gram_Ed4(IA);
UGramlayer_G5 = Gram_G5(IA);
UGramlayer_Ed5 = Gram_Ed5(IA);
UGramlayer_G6 = Gram_G6(IA);
UGram_layer = layer6(IA);
UGram_shape = Gram_shape(IA);


UGGL = union(UGramlayer_G6,union(UGramlayer_G5,union(union(UGramlayer_G3,UGramlayer_G4),union(UGramlayer_G1,UGramlayer_G2))));
UEEdL = union(UGramlayer_Ed5,union(union(UGramlayer_Ed1,UGramlayer_Ed2),union(UGramlayer_Ed3,UGramlayer_Ed4)));

savename = [temp '/partgram6_linkage_layer'];
save(savename,'gram6_layer','layer6','unique_gram6_layer','Gram_G1','Gram_Ed1','Gram_G2', ...
   'Gram_Ed2','Gram_G3','Gram_Ed3','Gram_G4','Gram_Ed4','Gram_G5','Gram_Ed5','Gram_G6', ...
   'UGram_layer','UGramlayer_G1','UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2', ...
   'UGramlayer_G3','UGramlayer_Ed3','UGramlayer_G4','UGramlayer_Ed4','UGramlayer_G5', ...
   'UGramlayer_Ed5','UGramlayer_G6','UGram_shape','UGGL','UEEdL');


