function generate_partlinkage2(temp)

% generate the linkages in 2-gram
load([temp '/Gram_2.mat']);

for i = 1: length(Gram_2)
   Gram_2{i} = Gram_2{i}(2:end-1);
end

gram2 = Gram_2;

for k = 1:length(gram2)
   aa = findstr(']',gram2{k});
   bb = findstr('+',gram2{k});
   layer2(k) = str2num(gram2{k}(1));
   if length(aa)==2
      gram2{k} =gram2{k}(aa(1)+1:end);
   else
      gram2{k} = gram2{k}(bb(1)+1:end);     
   end
   gram2_layer{k} = [num2str(layer2(k)) '-' gram2{k} ];
end
 
layer2 = layer2';
gram2_layer  = gram2_layer';

for i = 1:length(gram2)
   aa = findstr('[',gram2{i});
   bb = findstr(']',gram2{i});
   Gram_G1{i} = gram2{i}(1:aa-1);
   Gram_Ed{i} = gram2{i}(aa+1:bb-1);
   Gram_G2{i} = gram2{i}(bb+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed = Gram_Ed';
Gram_G2 = Gram_G2';

[unique_gram2_layer,IA] = unique(gram2_layer);

UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed = Gram_Ed(IA);
UGramlayer_G2= Gram_G2(IA);
UGram_layer = layer2(IA);

UGGL = union(UGramlayer_G1,UGramlayer_G2);
UEEdL = unique(UGramlayer_Ed);

savename = [temp '/partgram2_linkage_layer'];
save(savename,'gram2_layer','layer2','unique_gram2_layer', ...
   'Gram_G1','Gram_Ed','Gram_G2','UGram_layer','UGramlayer_G1', ...
   'UGramlayer_Ed','UGramlayer_G2','UGGL','UEEdL');




