function [classes,f]=featureselect2010(method,q,temp)
global K;
per = 0.6;


load([temp '/ground_truth_data.mat']);

[N,M]=size(ground_truth);
%% get the qgram list and the corresponding vector for each glycan
load ([temp '/Upart_gram' num2str(q) '_vector']);
All_qgram = {};
V = [];
for k =str2num(q)
   qgram = Upart_qgram{k};
   v = Upart_VV;
   All_qgram = [All_qgram qgram];
   V = [V;v];
end

load([temp '/Kernel/gram' q '_' method '.mat']);

%% svm training and test
data = (1:N)';
for k = 1:M
   options = optimset('maxiter',1000);
   svmStruct = svmtrain(data, ground_truth(:,k),'Kernel_Function',@(x,y)Kernelfun(x,y),'AUTOSCALE',0,...
      'Method','QP','quadprog_opts',options);
   [classes,f(:,k)] = mysvmclassify1(svmStruct,data,0);             
end

Predicted_score = -f;

%% feature selection
[Id1,Id2]=find(V);
for i=1:length(Id1)
   V(Id1(i),Id2(i))=1;
end
[Feature_score,Sorted_FID,SFID] =  feature_selection(V',Predicted_score,per);
for i = 1: M
   Feature_Selected{i}(:,1) = All_qgram(Sorted_FID{i})';  
   Feature_All{i}(:,1)=All_qgram(SFID(:,i));
   Feature_Selected{i}(:,2) = cellstr(num2str(Feature_score(Sorted_FID{i},i)));
   Feature_All{i}(:,2) = cellstr(num2str(Feature_score(SFID(:,i),i)));
   Score_All{i}=Feature_score(SFID(:,i),i);
end
savename = [temp '/Feature/Feature_' method '_gram' num2str(q)  '.mat'];
save(savename,'Feature_Selected','Feature_score','Sorted_FID','SFID','Feature_All','Score_All'); 




 
