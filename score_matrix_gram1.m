function score_matrix_gram1(alpha, temp, scin, maxl)
%% alpha represents the bioweight importance
% the similarity of linkages from 1-gram.
% generate the similarity between the linkages in 2-gram

load([temp '/partgram1_linkage_layer.mat']);
load([temp '/Linkagesimi.mat']);

Edgesimi =  (Edgesimi+ Edgesimi')/2;
Nodesimi = (Nodesimi+Nodesimi')/2;


[CG, IA,IB] = intersect(UGGL,Nodelist);
G_simi = eye(length(UGGL),length(UGGL));
G_simi(IA,IA) = Nodesimi(IB,IB);

for i = 1: length(unique_gram1_layer)
   n1(i) = find(strcmp(UGramlayer_G1{i},UGGL));
   l(i) = UGram_layer(i);
end

for i = 1: length(unique_gram1_layer)
   for j = 1: length(unique_gram1_layer)
      % UGramlayer_link_simi(i,j) = (1-abs(li-lj))*( G_simi(n1i,n1j) + G_simi(n2i,n2j) + Ed_simi(ei,ej))/3;
      Score_matrix(i,j) = (1-abs(l(i)-l(j))/maxl)*G_simi(n1(i),n1(j));
   end
end
gram = unique_gram1_layer;
BioWeight=zeros(length(gram),length(gram));
for i=1:length(gram)
   BioWeight(i,i)=exp(alpha*(str2num(gram{i}(1))));
end

savename=[temp '/ScoreMatrix/LK_gram1_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');

savename=[temp '/ScoreMatrix/LKR_gram1_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');


