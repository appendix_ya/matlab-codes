function create_gram3(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-3.txt'];
control = [temp '/control-3.txt'];
targetQ3 = textread(target, '%s ');
controlQ3 = textread(control, '%s ');

Gram_3=setdiff(union(targetQ3,controlQ3),entries);
Gram_3_vector=zeros(length(entries),length(Gram_3));
[WC,WIA,WIB]=intersect(entries,controlQ3);
for i=1:length(WIB)-1
   [uniquecontrolQ3,WIA3,WIB3]=unique(controlQ3(WIB(i)+1:WIB(i+1)-1));
   for k=1:length(uniquecontrolQ3)
      Freq1(k)=length(find(WIB3==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ3,Gram_3);
   Gram_3_vector(WIA(i),IB)=Freq1(IA);
end
[uniquecontrolQ3,WIA3,WIB3]=unique(controlQ3(WIB(end)+1:end));
for k=1:length(uniquecontrolQ3)
   Freq1(k)=length(find(WIB3==k));
end
[C,IA,IB]=intersect(uniquecontrolQ3,Gram_3);
Gram_3_vector(WIA(end),IB)=Freq1(IA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ3);
for i=1:length(FIB)-1
   [FC3,FIA3,FIB3]=unique(targetQ3(FIB(i)+1:FIB(i+1)-1));
   for k=1:length(FC3)
      Freq1(k)=length(find(FIB3==k));
   end
   [C,IA,IB]=intersect(FC3,Gram_3);
   Gram_3_vector(FIA(i),IB)=Freq1(IA);
end
[FC3,FIA3,FIB3]=unique(targetQ3(FIB(end)+1:end));
for k=1:length(FC3)
   Freq1(k)=length(find(FIB3==k));
end
[C,IA,IB]=intersect(FC3,Gram_3);
Gram_3_vector(FIA(end),IB)=Freq1(IA);
[rows,cols] = size(Gram_3);
Gram_3_weights = repmat(weights,1,rows);
Gram_3_vector = Gram_3_vector.*Gram_3_weights;

savename = [temp '/Gram_3'];
save(savename,'Gram_3','Gram_3_vector');



