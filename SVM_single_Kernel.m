function NAUC=SVM_single_Kernel(method,q,temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%method can be 
%'LK',link  q = 2,3
% set the parameters
% method = 'LK'; q = '3 '; 
runtime = 10;

% %DO NOT NEED CHAGE
balanced = 0;
normalized = 0;
foldnum = 5;
norm = 0;
load([temp '/ground_truth_data.mat']);

%%%%%%%%%%%%%%%%%%%%%%%%%
[N,M]=size(ground_truth);

global K;
load([temp '/Kernel/gram' q '_' method]);

%% normaliz or not
if normalized ==1 
   K = repmat(1./sqrt(diag(K)),1,size(K,1)).*K.*repmat((1./sqrt(diag(K)))',size(K,1),1); 
end

%% choose the class to do classfication
for i = 1:M a(i) = length(find(ground_truth(:,i)));end
   b = find(a>=10);

   %% runtimes SVM 
   data = (1:N)';
   for run = 1:runtime

   %Hao NEW 1212 %
      if runtime == 1
	load(['./mouse/fivefold_indices']);
      else
	indices = crossvalind('Kfold',data,foldnum);
      end

   f = zeros(size(indices));

   for k = 1:length(b)        
      for fold = 1:foldnum
         test = (indices == fold); %train = ~test;
         UnknownInd = find(test);
         train = ~test;           
         if balanced ==1
            IB = find(ground_truth(:,b(k)));
            NL = setdiff(find(train),IB);
            RNLA = NL(randperm(length(NL)));
            if length(NL)>2*length(intersect(find(train),IB))
               RNL = RNLA(1:length(intersect(find(train),IB)));
               train = union(IB,RNL);
            end
         end
         options = optimset('maxiter',1000);
         svmStruct = svmtrain(data(train), ground_truth(train,b(k)),'Kernel_Function',@(x,y)Kernelfun(x,y),'AUTOSCALE',0,...
                    'Method','QP','quadprog_opts',options);
         [classes,f(UnknownInd,k)] = mysvmclassify1(svmStruct,data(test,:),0);             
      end
   end
  
   Predicted_score = f;

   %%%%%calculating the AUC for the multiple classification
   PS = reshape(Predicted_score,N*length(b),1);
   GT = reshape(ground_truth(:,b),N*length(b),1);
   [PS_sorted,I] = sort(PS);
   GT_sorted = GT(I);
   NUM_1 = length(find(GT_sorted));
   NUM_0 = length(find(GT_sorted==0));
   S_N = sum(find(GT_sorted==0));
   AUC(run) = (S_N-NUM_0*(NUM_0+1)/2)/(NUM_1*NUM_0);

   %%% for figure
   if 0
      for i = 1:21
          cutoff = 0.1*(i-11);
          TI = find(PS_sorted<cutoff);
          num_1(i) = length(find(GT_sorted(TI)));
          num_0(i) = length(TI)-num_1(i);
          ROC_x(i) = num_0(i)/NUM_0;
          ROC_y(i) = num_1(i)/NUM_1;   
          Precision(i) = num_1(i)/length(TI);
          Recall(i) = num_1(i)/NUM_1;
      end
      clear num_1 num_0;
   end

   %%calculate the AUC for each classification
   for k = 1:length(b)
       PS = Predicted_score(:,k);
       GT = ground_truth(:,b(k));
       [PS_sorted,I] = sort(PS);
       GT_sorted = GT(I);
       NUM_1 = length(find(GT_sorted));
       NUM_0 = length(find(GT_sorted==0));
       S_N = sum(find(GT_sorted==0));
       NAUC(k,run) = (S_N-NUM_0*(NUM_0+1)/2)/(NUM_1*NUM_0);
       if 0
          for i = 1:21
              cutoff = 0.1*(i-11);
              TI = find(PS_sorted<cutoff);
              num_1(i) = length(find(GT_sorted(TI)));
              num_0(i) = length(TI)-num_1(i);
              NROC_x(k,i) = num_0(i)/NUM_0;
              NROC_y(k,i) = num_1(i)/NUM_1;   
              NPrecision(k,i) = num_1(i)/length(TI);
              NRecall(k,i) = num_1(i)/NUM_1;
          end
       end
    end

end

%% save data
savename = [temp '/ROC/partGram' q  '_' method  '_ROC'];
save(savename, 'Predicted_score','AUC','NAUC','balanced','normalized','foldnum');



