function [Feature_score,Selected_FID,SFID] =  feature_selection(X,DS, threshold)

%INPUT:
%X : column means objects,row means features
%decision_score, 
%threshold:percentage

%Output:
%feature_score:
num_feature =  size(X,1); % the number of features
num_class = size(DS,2);%the number of classes

Feature_score = X*DS;
[Sorted_Feature_score,SFID] = sort(-Feature_score,1);
Sorted_Feature_score = - Sorted_Feature_score;
max_score = Sorted_Feature_score(1,:);
for j = 1:num_class
      Selected_FID{j} = SFID(find(Sorted_Feature_score(:,j)>=max_score(j)*threshold),j);
end

if 0
   for j = 1: num_class
      for i = 1:num_feature
         Feature_score(i,j) =   (X(i,:)>=1)*DS(:,j);
      end        
      [Sorted,ID1] =  sort(-Feature_score(:,j));
      Sorted = -Sorted;
      %s = min(Sorted)+threshold*(max(Sorted)-min(Sorted));
      %Sorted_ID{j} = ID1(find(Sorted>=s));
      Sorted_ID{j} = ID1(find(Sorted>=max(Sorted)*threshold));
   end
end

  
