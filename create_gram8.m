function create_gram8(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=8
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-8.txt'];
control = [temp '/control-8.txt'];
targetQ8 = textread(target, '%s ');
controlQ8 = textread(control, '%s ');


Gram_8=setdiff(union(targetQ8,controlQ8),entries);
Gram_8_vector=zeros(length(entries),length(Gram_8));
[WC,WIA,WIB]=intersect(entries,controlQ8);
for i=1:length(WIB)-1
   [uniquecontrolQ8,WIA8,WIB8]=unique(controlQ8(WIB(i)+1:WIB(i+1)-1));
   if(length(WIB8)==0)
      Gram_8_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ8)
         Freq1(k)=length(find(WIB8==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ8,Gram_8);
      Gram_8_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ8,WIA8,WIB8]=unique(controlQ8(WIB(end)+1:end));
if(length(WIB8)==0)
   Gram_8_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ8)
      Freq1(k)=length(find(WIB8==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ8,Gram_8);
   Gram_8_vector(WIA(end),IB)=Freq1(IA);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ8);
for i=1:length(FIB)-1
   [FC8,FIA8,FIB8]=unique(targetQ8(FIB(i)+1:FIB(i+1)-1));
   if(length(FIB8)==0)
      Gram_8_vector(FIA(i),:)=0;
   else
      for k=1:length(FC8)
         Freq1(k)=length(find(FIB8==k));
      end
      [C,IA,IB]=intersect(FC8,Gram_8);
      Gram_8_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC8,FIA8,FIB8]=unique(targetQ8(FIB(end)+1:end));
if(length(FIB8)==0)
   Gram_8_vector(FIA(i),:)=0;
else
   for k=1:length(FC8)
      Freq1(k)=length(find(FIB8==k));
   end
   [C,IA,IB]=intersect(FC8,Gram_8);
   Gram_8_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_8);
Gram_8_weights = repmat(weights,1,rows);
Gram_8_vector = Gram_8_vector.*Gram_8_weights;

savename = [temp '/Gram_8'];
save(savename,'Gram_8','Gram_8_vector');



