function create_gram9(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=9
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-9.txt'];
control = [temp '/control-9.txt'];
targetQ9 = textread(target, '%s ');
controlQ9 = textread(control, '%s ');

Gram_9=setdiff(union(targetQ9,controlQ9),entries);
Gram_9_vector=zeros(length(entries),length(Gram_9));
[WC,WIA,WIB]=intersect(entries,controlQ9);
for i=1:length(WIB)-1
   [uniquecontrolQ9,WIA9,WIB9]=unique(controlQ9(WIB(i)+1:WIB(i+1)-1));
   if(length(WIB9)==0)
      Gram_9_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ9)
         Freq1(k)=length(find(WIB9==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ9,Gram_9);
      Gram_9_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ9,WIA9,WIB9]=unique(controlQ9(WIB(end)+1:end));
if(length(WIB9)==0)
   Gram_9_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ9)
      Freq1(k)=length(find(WIB9==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ9,Gram_9);
   Gram_9_vector(WIA(end),IB)=Freq1(IA);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ9);
for i=1:length(FIB)-1
   [FC9,FIA9,FIB9]=unique(targetQ9(FIB(i)+1:FIB(i+1)-1));
   if(length(FIB9)==0)
      Gram_9_vector(FIA(i),:)=0;
   else
      for k=1:length(FC9)
         Freq1(k)=length(find(FIB9==k));
      end
      [C,IA,IB]=intersect(FC9,Gram_9);
      Gram_9_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC9,FIA9,FIB9]=unique(targetQ9(FIB(end)+1:end));
if(length(FIB9)==0)
   Gram_9_vector(FIA(i),:)=0;
else
   for k=1:length(FC9)
      Freq1(k)=length(find(FIB9==k));
   end
   [C,IA,IB]=intersect(FC9,Gram_9);
   Gram_9_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_9);
Gram_9_weights = repmat(weights,1,rows);
Gram_9_vector = Gram_9_vector.*Gram_9_weights;

savename = [temp '/Gram_9'];
save(savename,'Gram_9','Gram_9_vector');





