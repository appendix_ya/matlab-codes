function create_gram4(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-4.txt'];
control = [temp '/control-4.txt'];
targetQ4 = textread(target, '%s ');
controlQ4 = textread(control, '%s ');

Gram_4=setdiff(union(targetQ4,controlQ4),entries);
Gram_4_vector=zeros(length(entries),length(Gram_4));
[WC,WIA,WIB]=intersect(entries,controlQ4);
for i=1:length(WIB)-1
   [uniquecontrolQ4,WIA4,WIB4]=unique(controlQ4(WIB(i)+1:WIB(i+1)-1));
   if (length(WIB4)==0)
      Gram_4_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ4)
         Freq1(k)=length(find(WIB4==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ4,Gram_4);
      Gram_4_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ4,WIA4,WIB4]=unique(controlQ4(WIB(end)+1:end));
if (length(WIB4)==0)
   Gram_4_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ4)
      Freq1(k)=length(find(WIB4==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ4,Gram_4);
   Gram_4_vector(WIA(end),IB)=Freq1(IA);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ4);
for i=1:length(FIB)-1
   [FC4,FIA4,FIB4]=unique(targetQ4(FIB(i)+1:FIB(i+1)-1));
   if (length(FIB4)==0)
      Gram_4_vector(FIA(i),:)=0;
   else
      for k=1:length(FC4)
         Freq1(k)=length(find(FIB4==k));
      end
      [C,IA,IB]=intersect(FC4,Gram_4);
      Gram_4_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC4,FIA4,FIB4]=unique(targetQ4(FIB(end)+1:end));
if (length(FIB4)==0)
   Gram_4_vector(FIA(i),:)=0;
else
   for k=1:length(FC4)
      Freq1(k)=length(find(FIB4==k));
   end
   [C,IA,IB]=intersect(FC4,Gram_4);
   Gram_4_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_4);
Gram_4_weights = repmat(weights,1,rows);
Gram_4_vector = Gram_4_vector.*Gram_4_weights;

savename = [temp '/Gram_4'];
save(savename,'Gram_4','Gram_4_vector');



