function create_gram1(temp)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-1.txt'];
control = [temp '/control-1.txt'];
targetQ1 = textread(target, '%s ');
controlQ1 = textread(control, '%s ');

Gram_1=setdiff(union(targetQ1,controlQ1),entries);
Gram_1_vector=zeros(length(entries),length(Gram_1));
[WC,WIA,WIB]=intersect(entries,controlQ1);

for i=1:length(WIB)-1
   [uniquecontrolQ1,WIA1,WIB1]=unique(controlQ1(WIB(i)+1:WIB(i+1)-1));
   for k=1:length(uniquecontrolQ1)
      Freq1(k)=length(find(WIB1==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ1,Gram_1);
   Gram_1_vector(WIA(i),IB)=Freq1(IA);
end

[uniquecontrolQ1,WIA1,WIB1]=unique(controlQ1(WIB(end)+1:end));

for k=1:length(uniquecontrolQ1)
   Freq1(k)=length(find(WIB1==k));
end

[C,IA,IB]=intersect(uniquecontrolQ1,Gram_1);
Gram_1_vector(WIA(end),IB)=Freq1(IA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ1);
for i=1:length(FIB)-1
   [FC1,FIA1,FIB1]=unique(targetQ1(FIB(i)+1:FIB(i+1)-1));
   for k=1:length(FC1)
      Freq1(k)=length(find(FIB1==k));
   end
   [C,IA,IB]=intersect(FC1,Gram_1);
   Gram_1_vector(FIA(i),IB)=Freq1(IA);
end

[FC1,FIA1,FIB1]=unique(targetQ1(FIB(end)+1:end));

for k=1:length(FC1)
   Freq1(k)=length(find(FIB1==k));
end
[C,IA,IB]=intersect(FC1,Gram_1);
Gram_1_vector(FIA(end),IB)=Freq1(IA);
 
[rows,cols] = size(Gram_1);
Gram_1_weights = repmat(weights,1,rows);
Gram_1_vector = Gram_1_vector.*Gram_1_weights;

savename = [temp '/Gram_1'];
save(savename,'Gram_1','Gram_1_vector');


