function generate_partlinkage1(temp)
% generate the linkages in 1-gram
load([temp '/Gram_1.mat']);

for i = 1: length(Gram_1)
   Gram_1{i} = Gram_1{i}(2:end-1);
end

for k = 1:length(Gram_1)
   aa = findstr(']',Gram_1{k});
   bb = findstr('+',Gram_1{k});
   layer1(k) = str2num(Gram_1{k}(1));
   if length(aa)==1
      Gram_1{k} =Gram_1{k}(aa(1)+1:end);
   else
      Gram_1{k} = Gram_1{k}(bb(1)+1:end);     
   end
   gram1_layer{k} = [num2str(layer1(k)) '+' Gram_1{k} ];
end
 
[unique_gram1_layer,IA] = unique(gram1_layer);

UGramlayer_G1 = Gram_1(IA);
UGram_layer = layer1(IA);

UGGL = unique(UGramlayer_G1);

savename = [temp '/partgram1_linkage_layer'];
save(savename,'layer1','unique_gram1_layer','Gram_1','UGramlayer_G1','UGram_layer','UGGL');


