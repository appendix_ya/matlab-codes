function main(tmp_in)

tmp = num2str(tmp_in);

diary([tmp '/matlabERROR.txt']);
param = [tmp '/param.txt'];
scomat = [tmp '/score_matrix.txt'];


fid=fopen(param,'r');
[params] = fscanf(fid, '%g %g %g %g %g %g');
fclose(fid);

alpha=params(1:1);      %alpha paramater
mNum=params(2:2);       %method number ((1)weighted q-gram (LK), (2)bio-weighted q-gram (LKR), (3)both)
minNum=params(3:3);     %minimum size of decomposed structure
maxNum=params(4:4);     %maximum size of decomposed structure
maxLsize=params(6:6);	%mthe number of maximum layer


fileid=fopen(scomat,'r');
ScIn = textscan(fileid, '%s : %s - %s : %s = %s');
fclose(fileid);


try 
   read_labels(tmp);

   Create_Gram(minNum,maxNum,tmp);

   Generate_Plink(minNum,maxNum,tmp);

   Remove_Gram(minNum,maxNum,tmp);

   main_classification_featureselection(alpha,mNum,tmp,minNum,maxNum,ScIn,maxLsize);
    
catch err
   rethrow(err);
end

diary('off');
clear;
%exit;
