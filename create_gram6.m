function create_gram6(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=6
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-6.txt'];
control = [temp '/control-6.txt'];
targetQ6 = textread(target, '%s ');
controlQ6 = textread(control, '%s ');

Gram_6=setdiff(union(targetQ6,controlQ6),entries);
Gram_6_vector=zeros(length(entries),length(Gram_6));
[WC,WIA,WIB]=intersect(entries,controlQ6);
for i=1:length(WIB)-1
   [uniquecontrolQ6,WIA6,WIB6]=unique(controlQ6(WIB(i)+1:WIB(i+1)-1));
   if(length(WIB6)==0)
      Gram_6_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ6)
         Freq1(k)=length(find(WIB6==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ6,Gram_6);
      Gram_6_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ6,WIA6,WIB6]=unique(controlQ6(WIB(end)+1:end));
if(length(WIB6)==0)
   Gram_6_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ6)
      Freq1(k)=length(find(WIB6==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ6,Gram_6);
   Gram_6_vector(WIA(end),IB)=Freq1(IA);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ6);
for i=1:length(FIB)-1
   [FC6,FIA6,FIB6]=unique(targetQ6(FIB(i)+1:FIB(i+1)-1));
   if(length(FIB6)==0)
      Gram_6_vector(FIA(i),:)=0;
   else
      for k=1:length(FC6)
         Freq1(k)=length(find(FIB6==k));
      end
      [C,IA,IB]=intersect(FC6,Gram_6);
      Gram_6_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC6,FIA6,FIB6]=unique(targetQ6(FIB(end)+1:end));
if(length(FIB6)==0)
   Gram_6_vector(FIA(i),:)=0;
else
   for k=1:length(FC6)
      Freq1(k)=length(find(FIB6==k));
   end
   [C,IA,IB]=intersect(FC6,Gram_6);
   Gram_6_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_6);
Gram_6_weights = repmat(weights,1,rows);
Gram_6_vector = Gram_6_vector.*Gram_6_weights;

savename = [temp '/Gram_6'];
save(savename,'Gram_6','Gram_6_vector');





