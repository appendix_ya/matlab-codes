function featurelist(temp,method,minq,maxq)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%Feature scoreing display

whole=0;
for i=minq:maxq
   load ([temp  '/Feature/Feature_' method '_gram' num2str(i)  '.mat']);

   whole1=whole+length(Feature_All{1});
   List(whole+1:whole1)=Feature_All{1}(:,1);
   Listscore(whole+1:whole1)=Score_All{1};
   whole=whole1;
end

[Sort_list,B]=sort(Listscore,'descend');
List=List(B);
Listscore=Sort_list;
LIST(:,1)=List';
LIST(:,2)=num2cell(Listscore');

savename = [temp  '/Featurelist/Feature_' method '.mat'];
save(savename,'List','Listscore','LIST'); 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% akune: for change charater %%%%%%%%%%%%%%%
tmp = LIST;
dir = [temp '/' method '_result.txt'];
mat2text(tmp, dir);



