function generate_partlinkage9(temp)

%% the similarity of linkages from 9-gram.
load([temp '/Gram_9.mat']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  remove the "";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gram9 = Gram_9;
for j = 1: length(gram9)
   gram9{j} = gram9{j}(2:end-1);
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% neglect the first redundant chemical bond;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:length(gram9)
   aa = findstr(']',gram9{k});
   bb = findstr('+',gram9{k});
   layer9(k) = str2num(gram9{k}(1));
   if length(aa)==9
      gram9{k} =gram9{k}(aa(1)+1:end);
   else
      gram9{k} = gram9{k}(bb(1)+1:end);     
   end
   gram9_layer{k} = [num2str(layer9(k)) '-' gram9{k} ];
end
 
layer9 = layer9';
gram9_layer  = gram9_layer';

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% unify the structure of 9-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(gram9)
   aa = findstr('<',gram9{i});
   bb = findstr('>',gram9{i});
   for j=0:100
      if length(aa)==j&&length(bb)==j
         Gram_shape(i)=j;
      end
   end
end

for i = 1:length(gram9)
   gram9{i}=strrep(gram9{i},'<','');
   gram9{i}=strrep(gram9{i},'>','');
   aa = findstr('[',gram9{i});
   bb = findstr(']',gram9{i});
   Gram_G1{i} = gram9{i}(1:aa(1)-1);
   Gram_Ed1{i} = gram9{i}(aa(1)+1:bb(1)-1);
   Gram_G2{i} = gram9{i}(bb(1)+1:aa(2)-1);
   Gram_Ed2{i} = gram9{i}(aa(2)+1:bb(2)-1);
   Gram_G3{i} = gram9{i}(bb(2)+1:aa(3)-1);
   Gram_Ed3{i} = gram9{i}(aa(3)+1:bb(3)-1);
   Gram_G4{i} = gram9{i}(bb(3)+1:aa(4)-1);
   Gram_Ed4{i} = gram9{i}(aa(4)+1:bb(4)-1);
   Gram_G5{i} = gram9{i}(bb(4)+1:aa(5)-1);
   Gram_Ed5{i} = gram9{i}(aa(5)+1:bb(5)-1);
   Gram_G6{i} = gram9{i}(bb(5)+1:aa(6)-1);
   Gram_Ed6{i} = gram9{i}(aa(6)+1:bb(6)-1);
   Gram_G7{i} = gram9{i}(bb(6)+1:aa(7)-1);
   Gram_Ed7{i} = gram9{i}(aa(7)+1:bb(7)-1);
   Gram_G8{i} = gram9{i}(bb(7)+1:aa(8)-1);
   Gram_Ed8{i} = gram9{i}(aa(8)+1:bb(8)-1);
   Gram_G9{i} = gram9{i}(bb(8)+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_Ed3 = Gram_Ed3';
Gram_G4 = Gram_G4';
Gram_Ed4 = Gram_Ed4';
Gram_G5 = Gram_G5';
Gram_Ed5 = Gram_Ed5';
Gram_G6 = Gram_G6';
Gram_Ed6 = Gram_Ed6';
Gram_G7 = Gram_G7';  
Gram_Ed7 = Gram_Ed7';
Gram_G8 = Gram_G8';  
Gram_Ed8 = Gram_Ed8';
Gram_G9 = Gram_G9'; 
Gram_shape = Gram_shape';
[unique_gram9_layer,IA] = unique(gram9_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGramlayer_Ed3 = Gram_Ed3(IA);
UGramlayer_G4 = Gram_G4(IA);
UGramlayer_Ed4 = Gram_Ed4(IA);
UGramlayer_G5 = Gram_G5(IA);
UGramlayer_Ed5 = Gram_Ed5(IA);
UGramlayer_G6 = Gram_G6(IA);
UGramlayer_Ed6 = Gram_Ed6(IA);
UGramlayer_G7 = Gram_G7(IA);
UGramlayer_Ed7 = Gram_Ed7(IA);
UGramlayer_G8 = Gram_G8(IA);
UGramlayer_Ed8 = Gram_Ed8(IA);
UGramlayer_G9 = Gram_G9(IA);
UGram_layer = layer9(IA);
UGram_shape = Gram_shape(IA);

UGGL = union(UGramlayer_G9,union(UGramlayer_G8,union(UGramlayer_G7,union(UGramlayer_G6,union(UGramlayer_G5,union(union(UGramlayer_G3,UGramlayer_G4),union(UGramlayer_G1,UGramlayer_G2)))))));
UEEdL = union(UGramlayer_Ed8,union(UGramlayer_Ed7,union(UGramlayer_Ed6,union(UGramlayer_Ed5,union(union(UGramlayer_Ed1,UGramlayer_Ed2),union(UGramlayer_Ed3,UGramlayer_Ed4))))));

savename = [temp '/partgram9_linkage_layer'];
save(savename,'gram9_layer','layer9','unique_gram9_layer','Gram_G1','Gram_Ed1','Gram_G2', ...
   'Gram_Ed2','Gram_G3','Gram_Ed3','Gram_G4','Gram_Ed4','Gram_G5','Gram_Ed5','Gram_G6', ...
   'Gram_Ed6','Gram_G7','Gram_Ed7','Gram_G8','Gram_Ed8','Gram_G9','UGram_layer', ...
   'UGramlayer_G1','UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2','UGramlayer_G3', ...
   'UGramlayer_Ed3','UGramlayer_G4','UGramlayer_Ed4','UGramlayer_G5','UGramlayer_Ed5', ...
   'UGramlayer_G6','UGramlayer_Ed6','UGramlayer_G7','UGramlayer_Ed7','UGramlayer_G8', ...
   'UGramlayer_Ed8','UGramlayer_G9','UGram_shape','UGGL','UEEdL');


