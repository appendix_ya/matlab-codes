function score_matrix_gram2(alpha, temp, scin, maxl)
%% alpha represents the bioweight importance
% the similarity of linkages from 2-gram.
% generate the similarity between the linkages in 2-gram

load([temp '/partgram2_linkage_layer.mat']);
load([temp '/Linkagesimi.mat']);

Edgesimi =  (Edgesimi+ Edgesimi')/2;
Nodesimi = (Nodesimi+Nodesimi')/2;

[CED, IA,IB] = intersect(UEEdL,Edgelist);
Ed_simi = eye(length(UEEdL),length(UEEdL));
Ed_simi(IA,IA) = Edgesimi(IB,IB);


[CG, IA,IB] = intersect(UGGL,Nodelist);
G_simi = eye(length(UGGL),length(UGGL));
G_simi(IA,IA) = Nodesimi(IB,IB);

for i = 1: length(unique_gram2_layer)
   n1(i) = find(strcmp(UGramlayer_G1{i},UGGL));
   n2(i) = find(strcmp(UGramlayer_G2{i},UGGL));
   e(i) = find(strcmp(UGramlayer_Ed{i},UEEdL));
   l(i) = UGram_layer(i);
end


scnum = length(scin{1,1});
Score_matrix = zeros(length(unique_gram2_layer),length(unique_gram2_layer));
for i = 1: length(unique_gram2_layer)
   for j = 1: length(unique_gram2_layer)
      Score_matrix(i,j) = (1-abs(l(i)-l(j))/maxl)*G_simi(n2(i),n2(j));
      ne1 = 0;

      for m = 1 : scnum
         if (isequal(UGramlayer_G1{i},char(scin{1,1}(m))) == 1) && (isequal(UGramlayer_Ed{i},char(scin{1,2}(m))) == 1) && (isequal(UGramlayer_G1{j},char(scin{1,3}(m))) == 1) && (isequal(UGramlayer_Ed{j},char(scin{1,4}(m))) == 1)
            ne1 = str2double(scin{1,5}(m));
         end
      end

      if ne1 == 0
         Score_matrix(i,j) = Score_matrix(i,j)*G_simi(n1(i),n1(j))*Ed_simi(e(i),e(j));
      else
         Score_matrix(i,j) = Score_matrix(i,j)*ne1;
      end
   end
   if ~mod(i,100)
      i
   end
end

Score_matrix(find(Score_matrix<1e-2)) = 0;
gram = unique_gram2_layer;
BioWeight=zeros(length(gram),length(gram));
for i=1:length(gram)
   BioWeight(i,i)=exp(alpha*(str2num(gram{i}(1))));
end

savename=[temp '/ScoreMatrix/LK_gram2_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');

savename=[temp '/ScoreMatrix/LKR_gram2_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');

