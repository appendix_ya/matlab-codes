function Creat_Kernel(method, q, temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% creat kernel for the dataset

load ([temp '/Upart_gram' q '_vector.mat']);
load ([temp '/ground_truth_data.mat']);

if strcmp(method,'LKR')
   load([temp '/ScoreMatrix/' method '_gram' num2str(q) '_Scorematrix']);

   V = Upart_VV;
   [X,P]=eig(Score_matrix);
   for i=1:size(P,1)
      if P(i,i)<=1
         P(i,i)=exp(P(i,i)-1);
      end
   end
   Score_matrix=X*P*X';
   K=V*BioWeight*Score_matrix*BioWeight*V';
   savename = [temp '/Kernel/gram' num2str(q) '_' method];
   save(savename,'K');

elseif strcmp(method,'LK')
   load([temp '/ScoreMatrix/' method '_gram' num2str(q) '_Scorematrix']);

   V = Upart_VV;
   A = Score_matrix*V';
   K = A'*A;
   savename = [temp '/Kernel/gram' num2str(q) '_' method];
   save(savename,'K');
end


