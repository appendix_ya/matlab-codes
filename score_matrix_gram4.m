function score_matrix_gram4(alpha, temp, scin, maxl)
%%%%%%%%%%%%%%%%%%%%%%%%%
%% alpha represents the bioweight importance
%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% creat the similarity matrix for 4-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/partgram4_linkage_layer.mat']);
load([temp '/Linkagesimi.mat']);

Edgesimi =  (Edgesimi+ Edgesimi')/2;
Nodesimi = (Nodesimi+Nodesimi')/2;

[CED, IA,IB] = intersect(UEEdL,Edgelist);
Ed_simi = eye(length(UEEdL),length(UEEdL));
Ed_simi(IA,IA) = Edgesimi(IB,IB);

[CG, IA,IB] = intersect(UGGL,Nodelist);
G_simi = eye(length(UGGL),length(UGGL));
G_simi(IA,IA) = Nodesimi(IB,IB);

num =length(unique_gram4_layer) ;
for i = 1: num
   n1(i) = find(strcmp(UGramlayer_G1{i},UGGL));
   n2(i) = find(strcmp(UGramlayer_G2{i},UGGL));
   n3(i) = find(strcmp(UGramlayer_G3{i},UGGL));
   n4(i) = find(strcmp(UGramlayer_G4{i},UGGL));
   e1(i) = find(strcmp(UGramlayer_Ed1{i},UEEdL));
   e2(i) = find(strcmp(UGramlayer_Ed2{i},UEEdL));
   e3(i) = find(strcmp(UGramlayer_Ed3{i},UEEdL));
   l(i) = UGram_layer(i);
   s(i) = UGram_shape(i);
end
      
scnum = length(scin{1,1});
Score_matrix = zeros(num,num);
for i = 1:num
   for j =1:num
      if isequal(s(i),s(j)) == 1
         Score_matrix(i,j) = (1-abs(l(i)-l(j))/maxl)*G_simi(n4(i),n4(j));
         ne1 = 0;
         ne2 = 0;
         ne3 = 0;

         for m = 1 : scnum
            if (isequal(UGramlayer_G1{i},char(scin{1,1}(m))) == 1) && (isequal(UGramlayer_Ed1{i},char(scin{1,2}(m))) == 1) && (isequal(UGramlayer_G1{j},char(scin{1,3}(m))) == 1) && (isequal(UGramlayer_Ed1{j},char(scin{1,4}(m))) == 1)
               ne1 = str2double(scin{1,5}(m));
            end
            if (isequal(UGramlayer_G2{i},char(scin{1,1}(m))) == 1) && (isequal(UGramlayer_Ed2{i},char(scin{1,2}(m))) == 1) && (isequal(UGramlayer_G2{j},char(scin{1,3}(m))) == 1) && (isequal(UGramlayer_Ed2{j},char(scin{1,4}(m))) == 1)
               ne2 = str2double(scin{1,5}(m));
            end
            if (isequal(UGramlayer_G3{i},char(scin{1,1}(m))) == 1) && (isequal(UGramlayer_Ed3{i},char(scin{1,2}(m))) == 1) && (isequal(UGramlayer_G3{j},char(scin{1,3}(m))) == 1) && (isequal(UGramlayer_Ed3{j},char(scin{1,4}(m))) == 1)
               ne3 = str2double(scin{1,5}(m));
            end
         end

         if ne1 == 0
            Score_matrix(i,j) = Score_matrix(i,j)*G_simi(n1(i),n1(j))*Ed_simi(e1(i),e1(j));
         else
            Score_matrix(i,j) = Score_matrix(i,j)*ne1;
         end

         if ne2 == 0
            Score_matrix(i,j) = Score_matrix(i,j)*G_simi(n2(i),n2(j))*Ed_simi(e2(i),e2(j));
         else
            Score_matrix(i,j) = Score_matrix(i,j)*ne2;
         end

         if ne3 == 0
            Score_matrix(i,j) = Score_matrix(i,j)*G_simi(n3(i),n3(j))*Ed_simi(e3(i),e3(j));
         else
            Score_matrix(i,j) = Score_matrix(i,j)*ne3;
         end

      else
         Score_matrix(i,j) = 0;
      end

   end
end
Score_matrix(find(Score_matrix<1e-2)) = 0;        
gram = unique_gram4_layer;       

BioWeight=zeros(length(gram),length(gram));
for i=1:length(gram)
   BioWeight(i,i)=exp(alpha*(str2num(gram{i}(1))));
end

savename=[temp '/ScoreMatrix/LK_gram4_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');

savename=[temp '/ScoreMatrix/LKR_gram4_Scorematrix'];
save(savename, 'Score_matrix','gram','BioWeight');

