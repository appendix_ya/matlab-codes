function generate_partlinkage4(temp)

%% the similarity of linkages from 4-gram.
load([temp '/Gram_4.mat']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  remove the "";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gram4 = Gram_4;
for j = 1: length(gram4)
   gram4{j} = gram4{j}(2:end-1);
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% neglect the first redundant chemical bond;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:length(gram4)
   aa = findstr(']',gram4{k});
   bb = findstr('+',gram4{k});
   layer4(k) = str2num(gram4{k}(1));
   if length(aa)==4
      gram4{k} =gram4{k}(aa(1)+1:end);
   else
      gram4{k} = gram4{k}(bb(1)+1:end);     
   end
   gram4_layer{k} = [num2str(layer4(k)) '-' gram4{k} ];
end
 
layer4 = layer4';
gram4_layer  = gram4_layer';

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% unify the structure of 4-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:length(gram4)
   aa = findstr('<',gram4{i});
   bb = findstr('>',gram4{i});
   for j=0:100
      if length(aa)==j&&length(bb)==j
         Gram_shape(i)=j;
      end
   end
end

for i = 1:length(gram4)
   gram4{i}=strrep(gram4{i},'<','');
   gram4{i}=strrep(gram4{i},'>','');
   aa = findstr('[',gram4{i});
   bb = findstr(']',gram4{i});
   Gram_G1{i} = gram4{i}(1:aa(1)-1);
   Gram_Ed1{i} = gram4{i}(aa(1)+1:bb(1)-1);
   Gram_G2{i} = gram4{i}(bb(1)+1:aa(2)-1);
   Gram_Ed2{i} = gram4{i}(aa(2)+1:bb(2)-1);
   Gram_G3{i} = gram4{i}(bb(2)+1:aa(3)-1);
   Gram_Ed3{i} = gram4{i}(aa(3)+1:bb(3)-1);
   Gram_G4{i} = gram4{i}(bb(3)+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_Ed3 = Gram_Ed3';
Gram_G4 = Gram_G4';
Gram_shape=Gram_shape';
 
[unique_gram4_layer,IA] = unique(gram4_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGramlayer_Ed3 = Gram_Ed3(IA);
UGramlayer_G4= Gram_G4(IA);
UGram_layer = layer4(IA);
UGram_shape = Gram_shape(IA);


UGGL = union(union(UGramlayer_G3,UGramlayer_G4),union(UGramlayer_G1,UGramlayer_G2));
UEEdL = union(UGramlayer_Ed1,union(UGramlayer_Ed2,UGramlayer_Ed3));

savename = [temp '/partgram4_linkage_layer'];
save(savename,'gram4_layer','layer4','unique_gram4_layer','Gram_G1','Gram_Ed1', ...
   'Gram_G2','Gram_Ed2','Gram_G3','Gram_Ed3','Gram_G4','UGram_layer', ...
   'UGramlayer_G1','UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2', ...
   'UGramlayer_G3','UGramlayer_Ed3','UGramlayer_G4','UGram_shape','UGGL','UEEdL');




