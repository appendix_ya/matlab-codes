function create_gram5(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Create q-gram for the dataset: q=5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load([temp '/ground_truth_data.mat']);

target = [temp '/target-5.txt'];
control = [temp '/control-5.txt'];
targetQ5 = textread(target, '%s ');
controlQ5 = textread(control, '%s ');

Gram_5=setdiff(union(targetQ5,controlQ5),entries);
Gram_5_vector=zeros(length(entries),length(Gram_5));
[WC,WIA,WIB]=intersect(entries,controlQ5);
for i=1:length(WIB)-1
   [uniquecontrolQ5,WIA5,WIB5]=unique(controlQ5(WIB(i)+1:WIB(i+1)-1));
   if(length(WIB5)==0)
      Gram_5_vector(WIA(i),:)=0;
   else
      for k=1:length(uniquecontrolQ5)
         Freq1(k)=length(find(WIB5==k));
      end
      [C,IA,IB]=intersect(uniquecontrolQ5,Gram_5);
      Gram_5_vector(WIA(i),IB)=Freq1(IA);
   end
end
[uniquecontrolQ5,WIA5,WIB5]=unique(controlQ5(WIB(end)+1:end));
if(length(WIB5)==0)
   Gram_5_vector(WIA(i),:)=0;
else
   for k=1:length(uniquecontrolQ5)
      Freq1(k)=length(find(WIB5==k));
   end
   [C,IA,IB]=intersect(uniquecontrolQ5,Gram_5);
   Gram_5_vector(WIA(end),IB)=Freq1(IA);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[FC,FIA,FIB]=intersect(entries,targetQ5);
for i=1:length(FIB)-1
   [FC5,FIA5,FIB5]=unique(targetQ5(FIB(i)+1:FIB(i+1)-1));
   if(length(FIB5)==0)
      Gram_5_vector(FIA(i),:)=0;
   else
      for k=1:length(FC5)
         Freq1(k)=length(find(FIB5==k));
      end
      [C,IA,IB]=intersect(FC5,Gram_5);
      Gram_5_vector(FIA(i),IB)=Freq1(IA);
   end
end
[FC5,FIA5,FIB5]=unique(targetQ5(FIB(end)+1:end));
if(length(FIB5)==0)
   Gram_5_vector(FIA(i),:)=0;
else
   for k=1:length(FC5)
      Freq1(k)=length(find(FIB5==k));
   end
   [C,IA,IB]=intersect(FC5,Gram_5);
   Gram_5_vector(FIA(end),IB)=Freq1(IA);
end
[rows,cols] = size(Gram_5);
Gram_5_weights = repmat(weights,1,rows);
Gram_5_vector = Gram_5_vector.*Gram_5_weights;

savename = [temp '/Gram_5'];
save(savename,'Gram_5','Gram_5_vector');



