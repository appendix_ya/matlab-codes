function generate_partlinkage5(temp)

% the linkages from 5-gram.
load([temp '/Gram_5.mat']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  remove the "";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gram5 = Gram_5;
for j = 1: length(gram5)
   gram5{j} = gram5{j}(2:end-1);
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% neglect the first redundant chemical bond;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:length(gram5)
   aa = findstr(']',gram5{k});
   bb = findstr('+',gram5{k});
   layer5(k) = str2num(gram5{k}(1));
   if length(aa)==5
      gram5{k} =gram5{k}(aa(1)+1:end);
   else
      gram5{k} = gram5{k}(bb(1)+1:end);     
   end
   gram5_layer{k} = [num2str(layer5(k)) '-' gram5{k} ];
end
 
layer5 = layer5';
gram5_layer  = gram5_layer';

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% unify the structure of 5-grams
%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(gram5)
   aa = findstr('<',gram5{i});
   bb = findstr('>',gram5{i});
   for j=0:100
      if length(aa)==j&&length(bb)==j
         Gram_shape(i)=j;
      end
   end
end


for i = 1:length(gram5)
   gram5{i}=strrep(gram5{i},'<','');
   gram5{i}=strrep(gram5{i},'>','');
   aa = findstr('[',gram5{i});
   bb = findstr(']',gram5{i});
   Gram_G1{i} = gram5{i}(1:aa(1)-1);
   Gram_Ed1{i} = gram5{i}(aa(1)+1:bb(1)-1);
   Gram_G2{i} = gram5{i}(bb(1)+1:aa(2)-1);
   Gram_Ed2{i} = gram5{i}(aa(2)+1:bb(2)-1);
   Gram_G3{i} = gram5{i}(bb(2)+1:aa(3)-1);
   Gram_Ed3{i} = gram5{i}(aa(3)+1:bb(3)-1);
   Gram_G4{i} = gram5{i}(bb(3)+1:aa(4)-1);
   Gram_Ed4{i} = gram5{i}(aa(4)+1:bb(4)-1);
   Gram_G5{i} = gram5{i}(bb(4)+1:end);
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_Ed3 = Gram_Ed3';
Gram_G4 = Gram_G4';
Gram_Ed4 = Gram_Ed4';
Gram_G5 = Gram_G5';
Gram_shape=Gram_shape';
  
[unique_gram5_layer,IA] = unique(gram5_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGramlayer_Ed3 = Gram_Ed3(IA);
UGramlayer_G4 = Gram_G4(IA);
UGramlayer_Ed4 = Gram_Ed4(IA);
UGramlayer_G5 = Gram_G5(IA);
UGram_layer = layer5(IA);
UGram_shape = Gram_shape(IA);


UGGL = union(UGramlayer_G5,union(union(UGramlayer_G3,UGramlayer_G4),union(UGramlayer_G1,UGramlayer_G2)));
UEEdL = union(union(UGramlayer_Ed1,UGramlayer_Ed2),union(UGramlayer_Ed3,UGramlayer_Ed4));

savename = [temp '/partgram5_linkage_layer'];
save(savename,'gram5_layer','layer5','unique_gram5_layer','Gram_G1','Gram_Ed1','Gram_G2', ...
   'Gram_Ed2','Gram_G3','Gram_Ed3','Gram_G4','Gram_Ed4','Gram_G5','UGram_layer', ...
   'UGramlayer_G1','UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2','UGramlayer_G3', ...
   'UGramlayer_Ed3','UGramlayer_G4','UGramlayer_Ed4','UGramlayer_G5','UGram_shape','UGGL','UEEdL');





