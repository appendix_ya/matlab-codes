function read_labels(temp)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%%  Read the labels of mouse-brain glycans to groud_truth_mouse.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

label = [temp '/labels.txt'];
complete_labels = textread(label, '%s ');

Ground_truth=complete_labels(2:2:end);
ground_truth=zeros(size(Ground_truth));
for i=1:length(Ground_truth)
   ground_truth(i)=strcmp(Ground_truth(i),'1');
end
entries = complete_labels(1:2:end);
[entries,B]=sort(entries);
ground_truth=ground_truth(B);

weight = [temp '/weights.txt'];
[weightstext, weights] = textread(weight, '%s %f ');

weights=weights(B);
%%weights=weights*100;
%%weights=weights*10;

savename = [temp '/ground_truth_data'];
save(savename,'ground_truth','entries','weights');


