function generate_partlinkage3(temp)

% generate the linkages in 3-gram
load([temp '/Gram_3.mat']);

gram3 = Gram_3;
for j = 1: length(gram3)
   gram3{j} = gram3{j}(2:end-1);
end

for k = 1:length(gram3)
   aa = findstr(']',gram3{k});
   bb = findstr('+',gram3{k});
   layer3(k) = str2num(gram3{k}(1));
   if length(aa)==3
      gram3{k} =gram3{k}(aa(1)+1:end);
   else
      gram3{k} = gram3{k}(bb(1)+1:end);     
   end
   gram3_layer{k} = [num2str(layer3(k)) '-' gram3{k} ];
end
 
layer3 = layer3';
gram3_layer  = gram3_layer';

for i = 1:length(gram3)
   aa = findstr('[',gram3{i});
   bb = findstr(']',gram3{i});
   cc = findstr('<',gram3{i});
   dd = findstr('>',gram3{i});
   if length(cc)==2 && length(dd)==2 
      Gram_G1{i} = gram3{i}(1:cc(1)-1);
      Gram_Ed1{i} = gram3{i}(aa(1)+1:bb(1)-1);
      Gram_G2{i} = gram3{i}(bb(1)+1:dd(1)-1);
      Gram_Ed2{i} = gram3{i}(aa(2)+1:bb(2)-1);
      Gram_G3{i} = gram3{i}(bb(2)+1:end-1);
      Gram_shape(i) = 1; %branched
   elseif  length(cc)==0 && length(dd)==0
      Gram_G1{i} = gram3{i}(1:aa(1)-1);
      Gram_Ed1{i} = gram3{i}(aa(1)+1:bb(1)-1);
      Gram_G2{i} = gram3{i}(bb(1)+1:aa(2)-1);
      Gram_Ed2{i} = gram3{i}(aa(2)+1:bb(2)-1);
      Gram_G3{i} = gram3{i}(bb(2)+1:end);
      Gram_shape(i) = 0; %straigt
   end
end

Gram_G1 = Gram_G1';
Gram_Ed1 = Gram_Ed1';
Gram_G2 = Gram_G2';
Gram_Ed2 = Gram_Ed2';
Gram_G3 = Gram_G3';
Gram_shape =  Gram_shape';
 
[unique_gram3_layer,IA] = unique(gram3_layer);


UGramlayer_G1 = Gram_G1(IA);
UGramlayer_Ed1 = Gram_Ed1(IA);
UGramlayer_G2 = Gram_G2(IA);
UGramlayer_Ed2 = Gram_Ed2(IA);
UGramlayer_G3 = Gram_G3(IA);
UGram_layer = layer3(IA);
UGram_shape = Gram_shape(IA);


UGGL = union(UGramlayer_G1,union(UGramlayer_G2,UGramlayer_G3));
UEEdL = union(UGramlayer_Ed1,UGramlayer_Ed2);

savename = [temp '/partgram3_linkage_layer'];
save(savename,'gram3_layer','layer3','unique_gram3_layer','Gram_G1', ...
   'Gram_Ed1','Gram_G2','Gram_Ed2','Gram_G3','UGram_layer', ...
   'UGramlayer_G1','UGramlayer_Ed1','UGramlayer_G2','UGramlayer_Ed2', ...
   'UGramlayer_G3','UGram_shape','UGGL','UEEdL');



